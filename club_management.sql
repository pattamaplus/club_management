-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 28, 2018 at 12:28 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `club_management`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_check_student`
--

CREATE TABLE `tbl_check_student` (
  `id` int(11) NOT NULL,
  `cid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `create_by` varchar(255) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_by` varchar(255) DEFAULT NULL,
  `update_date` timestamp NULL DEFAULT NULL,
  `delete_status` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_check_student`
--

INSERT INTO `tbl_check_student` (`id`, `cid`, `sid`, `create_by`, `create_date`, `update_by`, `update_date`, `delete_status`) VALUES
(23, 5, 25, '4', '2018-10-28 04:45:17', NULL, NULL, b'0'),
(24, 5, 24, '4', '2018-10-28 04:45:17', NULL, NULL, b'0'),
(28, 7, 25, '9', '2018-10-28 11:26:11', NULL, NULL, b'0');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_clubs`
--

CREATE TABLE `tbl_clubs` (
  `cid` int(11) NOT NULL,
  `clubname` varchar(255) NOT NULL,
  `day` varchar(50) NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  `create_by` varchar(255) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_by` varchar(255) DEFAULT NULL,
  `update_date` timestamp NULL DEFAULT NULL,
  `delete_status` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_clubs`
--

INSERT INTO `tbl_clubs` (`cid`, `clubname`, `day`, `start_time`, `end_time`, `create_by`, `create_date`, `update_by`, `update_date`, `delete_status`) VALUES
(5, 'pingpong', 'monday', '01:00:00', '03:00:00', '4', '2018-10-27 06:31:17', NULL, NULL, b'0'),
(6, 'tennis', 'tuesday', '03:00:00', '05:00:00', '4', '2018-10-27 06:33:36', NULL, NULL, b'0'),
(7, 'batminton', 'wednesday', '09:00:00', '11:00:00', '4', '2018-10-27 13:18:41', NULL, NULL, b'0'),
(8, 'swim', 'wednesday', '10:00:00', '00:00:00', '4', '2018-10-28 09:10:52', NULL, NULL, b'0');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_club_reltionship`
--

CREATE TABLE `tbl_club_reltionship` (
  `id` int(11) NOT NULL,
  `cid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `tid` int(11) NOT NULL,
  `taid` int(11) NOT NULL,
  `pid` int(11) NOT NULL,
  `create_by` varchar(255) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_by` varchar(255) DEFAULT NULL,
  `update_date` timestamp NULL DEFAULT NULL,
  `delete_status` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_club_reltionship`
--

INSERT INTO `tbl_club_reltionship` (`id`, `cid`, `sid`, `tid`, `taid`, `pid`, `create_by`, `create_date`, `update_by`, `update_date`, `delete_status`) VALUES
(61, 7, 25, 20, 47, 5, '4', '2018-10-28 05:29:14', NULL, NULL, b'0'),
(62, 7, 25, 20, 47, 4, '4', '2018-10-28 05:29:14', NULL, NULL, b'0'),
(63, 7, 25, 20, 46, 5, '4', '2018-10-28 05:29:14', NULL, NULL, b'0'),
(64, 7, 25, 20, 46, 4, '4', '2018-10-28 05:29:14', NULL, NULL, b'0'),
(65, 7, 24, 20, 47, 5, '4', '2018-10-28 05:29:14', NULL, NULL, b'0'),
(66, 7, 24, 20, 47, 4, '4', '2018-10-28 05:29:14', NULL, NULL, b'0'),
(67, 7, 24, 20, 46, 5, '4', '2018-10-28 05:29:14', NULL, NULL, b'0'),
(68, 7, 24, 20, 46, 4, '4', '2018-10-28 05:29:14', NULL, NULL, b'0'),
(69, 5, 25, 20, 47, 5, '4', '2018-10-28 05:33:02', NULL, NULL, b'1'),
(70, 5, 25, 20, 47, 5, '4', '2018-10-28 05:33:25', NULL, NULL, b'0');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_partners`
--

CREATE TABLE `tbl_partners` (
  `pid` int(11) NOT NULL,
  `partner_name` varchar(255) NOT NULL,
  `aptitude` varchar(255) NOT NULL,
  `contact_person` varchar(255) NOT NULL,
  `tel` varchar(10) NOT NULL,
  `email` varchar(255) NOT NULL,
  `create_by` varchar(255) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_by` varchar(255) DEFAULT NULL,
  `update_date` timestamp NULL DEFAULT NULL,
  `delete_status` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_partners`
--

INSERT INTO `tbl_partners` (`pid`, `partner_name`, `aptitude`, `contact_person`, `tel`, `email`, `create_by`, `create_date`, `update_by`, `update_date`, `delete_status`) VALUES
(4, 'ruymak', 'ruymak', 'nunsi', '0852314568', 'ruymak@gmail.com', '4', '2018-10-28 04:47:23', NULL, NULL, b'0'),
(5, 'grab', 'driver', 'joox', '0546985357', 'grab@gmail.com', '4', '2018-10-28 04:49:01', NULL, NULL, b'0');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_students`
--

CREATE TABLE `tbl_students` (
  `sid` int(11) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `year` int(11) NOT NULL,
  `create_by` int(255) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_by` varchar(255) DEFAULT NULL,
  `update_date` timestamp NULL DEFAULT NULL,
  `delete_status` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_students`
--

INSERT INTO `tbl_students` (`sid`, `firstname`, `lastname`, `year`, `create_by`, `create_date`, `update_by`, `update_date`, `delete_status`) VALUES
(24, 'wannapon', 'saisak', 1, 4, '2018-10-28 09:06:39', NULL, NULL, b'0'),
(25, 'pattamapron', 'mooaon', 0, 4, '2018-10-28 08:40:58', NULL, NULL, b'0');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_tas`
--

CREATE TABLE `tbl_tas` (
  `taid` int(11) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `tel` varchar(10) NOT NULL,
  `email` varchar(255) NOT NULL,
  `create_by` varchar(255) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_by` varchar(255) DEFAULT NULL,
  `update_date` timestamp NULL DEFAULT NULL,
  `delete_status` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_tas`
--

INSERT INTO `tbl_tas` (`taid`, `firstname`, `lastname`, `tel`, `email`, `create_by`, `create_date`, `update_by`, `update_date`, `delete_status`) VALUES
(46, 'manop', 'masai', '0952151456', 'manop@gmail.com', '4', '2018-10-28 06:16:46', NULL, NULL, b'0'),
(47, 'doodee', 'meechai', '0857453658', 'doodee@gmail.com', '4', '2018-10-28 04:36:39', NULL, NULL, b'0'),
(48, 'manop1', 'masai', '0952151456', 'manop@gmail.com', '4', '2018-10-28 06:11:21', NULL, NULL, b'1'),
(49, 'manop1', 'masai', '0952151456', 'manop@gmail.com', '4', '2018-10-28 06:12:30', NULL, NULL, b'1'),
(50, '', '', '', '', '4', '2018-10-28 06:31:28', NULL, NULL, b'1'),
(51, '', '', '', '', '9', '2018-10-28 11:02:25', NULL, NULL, b'0');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_teachers`
--

CREATE TABLE `tbl_teachers` (
  `tid` int(11) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `tel` varchar(10) NOT NULL,
  `email` varchar(255) NOT NULL,
  `create_by` varchar(255) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_by` varchar(255) DEFAULT NULL,
  `update_date` timestamp NULL DEFAULT NULL,
  `delete_status` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_teachers`
--

INSERT INTO `tbl_teachers` (`tid`, `firstname`, `lastname`, `tel`, `email`, `create_by`, `create_date`, `update_by`, `update_date`, `delete_status`) VALUES
(19, 'sukjai', 'kayandee', '0852457896', 'suljai@gmail.com', '4', '2018-10-28 04:14:10', NULL, NULL, b'0'),
(20, 'jaidee', 'jungloi', '0547896325', 'jaidee@gmail.com', '4', '2018-10-28 04:36:46', NULL, NULL, b'0');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `uid` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_by` varchar(255) DEFAULT NULL,
  `update_date` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `delete_status` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`uid`, `username`, `password`, `create_date`, `update_by`, `update_date`, `delete_status`) VALUES
(9, 'pattamaplus@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '2018-10-28 09:40:35', NULL, '0000-00-00 00:00:00', b'0');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_check_student`
--
ALTER TABLE `tbl_check_student`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_clubs`
--
ALTER TABLE `tbl_clubs`
  ADD PRIMARY KEY (`cid`);

--
-- Indexes for table `tbl_club_reltionship`
--
ALTER TABLE `tbl_club_reltionship`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_partners`
--
ALTER TABLE `tbl_partners`
  ADD PRIMARY KEY (`pid`);

--
-- Indexes for table `tbl_students`
--
ALTER TABLE `tbl_students`
  ADD PRIMARY KEY (`sid`);

--
-- Indexes for table `tbl_tas`
--
ALTER TABLE `tbl_tas`
  ADD PRIMARY KEY (`taid`);

--
-- Indexes for table `tbl_teachers`
--
ALTER TABLE `tbl_teachers`
  ADD PRIMARY KEY (`tid`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`uid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_check_student`
--
ALTER TABLE `tbl_check_student`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `tbl_clubs`
--
ALTER TABLE `tbl_clubs`
  MODIFY `cid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tbl_club_reltionship`
--
ALTER TABLE `tbl_club_reltionship`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;
--
-- AUTO_INCREMENT for table `tbl_partners`
--
ALTER TABLE `tbl_partners`
  MODIFY `pid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_students`
--
ALTER TABLE `tbl_students`
  MODIFY `sid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `tbl_tas`
--
ALTER TABLE `tbl_tas`
  MODIFY `taid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT for table `tbl_teachers`
--
ALTER TABLE `tbl_teachers`
  MODIFY `tid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `uid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
