const pool = require("../db");

class Club {
  constructor(row) {
    this.id = row.id;
    this.firstname = row.firstname;
    this.lastname = row.lastname;
    this.year = row.year;
    this.day = row.day;
    this.start_time = row.start_time;
    this.end_time = row.end_time;
    this.aptitude = row.aptitude;
    this.contact_person = row.contact_person;
    this.telephone = row.tel;
    this.email = row.email;
    this.create_date = row.create_date;
    this.user_type = row.user_type;
    this.radio_checked = row.radio_checked;
    this.clubname = row.clubname;
    this.student_name = row.student_name;
    this.teacher_name = row.teacher_name;
    this.ta_name = row.ta_name;
    this.checktype = row.checktype;
  }
}

class ClubName {
  constructor(row) {
    this.cid = row.cid;
    this.clubname = row.clubname;
  }
}

class Teacher {
  constructor(row) {
    this.tid = row.tid;
    this.teacher_name = row.teacher_name;
  }
}

class Student {
  constructor(row) {
    this.sid = row.sid;
    this.student_name = row.student_name;
  }
}

class TA {
  constructor(row) {
    this.taid = row.taid;
    this.ta_name = row.ta_name;
  }
}

class Partner {
  constructor(row) {
    this.pid = row.pid;
    this.partner_name = row.partner_name;
  }
}

class StudentDetail {
  constructor(row) {
    this.student_detail = row.student_detail;
    this.clubname_detail = row.clubname_detail;
    this.teacher_detail = row.teacher_detail;
    this.day_detail = row.day_detail;
  }
}

class TaDetail {
  constructor(row) {
    this.ta_detail = row.ta_detail;
  }
}

class PartnerDetail {
  constructor(row) {
    this.partner_detail = row.partner_detail;
  }
}

class CountStudent {
  constructor(row) {
    this.check_club = row.check_club;
  }
}

async function registerTAOrTeacher(
  ta_firstname,
  ta_lastname,
  ta_telephone,
  ta_email,
  create_by,
  checktype
) {
  if (checktype == "TA") {
    const result = await pool.query(
      `
          insert into tbl_tas
              (firstname,lastname,tel,email,create_by)
          values
              (?, ?, ?, ?, ?)
      `,
      [ta_firstname, ta_lastname, ta_telephone, ta_email, create_by]
    );
    return result[0].insertId;
  } else if (checktype == "Teacher") {
    const result = await pool.query(
      `
          insert into tbl_teachers
              (firstname,lastname,tel,email,create_by)
          values
              (?, ?, ?, ?, ?)
      `,
      [ta_firstname, ta_lastname, ta_telephone, ta_email, create_by]
    );
    return result[0].insertId;
  }
}

async function registerStudent(
  student_firstname,
  student_lastname,
  student_year,
  create_by
) {
  const result = await pool.query(
    `
        insert into tbl_students
            (firstname,lastname,year,create_by)
        values
            (?, ?, ?, ?)
    `,
    [student_firstname, student_lastname, student_year, create_by]
  );
  return result[0].insertId;
}

async function registerCheckStudent(
  ddl_check_clubname,
  ddl_check_student,
  create_by
) {
  let result;
  if (typeof ddl_check_student == "string") {
    result = await registerRowCheckStudent(
      ddl_check_clubname,
      ddl_check_student,
      create_by
    );
    return result;
  } else {
    await ddl_check_student.forEach(student => {
      result = registerRowCheckStudent(ddl_check_clubname, student, create_by);
    });
    return result;
  }
}

async function registerRowCheckStudent(
  ddl_check_clubname,
  ddl_check_student,
  create_by
) {
  const result = await pool.query(
    `
        insert into tbl_check_student
            (cid,sid,create_by)
        values
            (?, ?, ?)
    `,
    [ddl_check_clubname, ddl_check_student, create_by]
  );
  return result[0].insertId;
}
async function registerClub(
  clubname,
  club_day,
  start_time,
  end_time,
  create_by
) {
  const result = await pool.query(
    `
        insert into tbl_clubs
            (clubname,day,start_time,end_time,create_by)
        values
            (?, ?, ?, ?, ?)
    `,
    [clubname, club_day, start_time, end_time, create_by]
  );
  return result[0].insertId;
}

async function registerPartner(
  partner_name,
  aptitude,
  personal_contact,
  partner_tel,
  partner_email,
  create_by
) {
  const result = await pool.query(
    `
        insert into tbl_partners
            (partner_name,aptitude,contact_person,tel,email,create_by)
        values
            (?, ?, ?, ?, ?, ?)
    `,
    [
      partner_name,
      aptitude,
      personal_contact,
      partner_tel,
      partner_email,
      create_by
    ]
  );
  return result[0].insertId;
}

async function registerRelationship(
  ddl_clubname,
  ddl_teacher,
  ddl_student,
  ddl_ta,
  ddl_partner,
  create_by
) {
  let result;
  if (typeof ddl_student == "string") {
    if (typeof ddl_ta == "string") {
      if (typeof ddl_partner == "string") {
        result = await registerRowRelationship(
          ddl_clubname,
          ddl_teacher,
          ddl_student,
          ddl_ta,
          ddl_partner,
          create_by
        );
        return result;
      } else {
        result = await ddl_partner.forEach(partner => {
          registerRowRelationship(
            ddl_clubname,
            ddl_teacher,
            ddl_student,
            ddl_ta,
            partner,
            create_by
          );
        });
        return result;
      }
    } else {
      await ddl_ta.forEach(ta => {
        if (typeof ddl_partner == "string") {
          result = registerRowRelationship(
            ddl_clubname,
            ddl_teacher,
            ddl_student,
            ta,
            ddl_partner,
            create_by
          );
        } else {
          result = ddl_partner.forEach(partner => {
            registerRowRelationship(
              ddl_clubname,
              ddl_teacher,
              ddl_student,
              ta,
              partner,
              create_by
            );
          });
        }
      });
      return result;
    }
  } else {
    await ddl_student.forEach(student => {
      if (typeof ddl_ta == "string") {
        if (typeof ddl_partner == "string") {
          result = registerRowRelationship(
            ddl_clubname,
            ddl_teacher,
            student,
            ddl_ta,
            ddl_partner,
            create_by
          );
        } else {
          result = ddl_partner.forEach(partner => {
            registerRowRelationship(
              ddl_clubname,
              ddl_teacher,
              student,
              ddl_ta,
              partner,
              create_by
            );
          });
        }
      } else {
        ddl_ta.forEach(ta => {
          if (typeof ddl_partner == "string") {
            result = registerRowRelationship(
              ddl_clubname,
              ddl_teacher,
              student,
              ta,
              ddl_partner,
              create_by
            );
          } else {
            result = ddl_partner.forEach(partner => {
              registerRowRelationship(
                ddl_clubname,
                ddl_teacher,
                student,
                ta,
                partner,
                create_by
              );
            });
          }
        });
      }
    });
    return result;
  }
}

async function registerRowRelationship(
  ddl_clubname,
  ddl_teacher,
  ddl_student,
  ddl_ta,
  ddl_partner,
  create_by
) {
  const result = await pool.query(
    `
        insert into tbl_club_reltionship
            (cid,tid,sid,taid,pid,create_by)
        values
            (?, ?, ?, ?, ?, ?)
    `,
    [ddl_clubname, ddl_teacher, ddl_student, ddl_ta, ddl_partner, create_by]
  );
  return result[0].insertId;
}

async function listClub() {
  const [rows] = await pool.query(
    `SELECT cid,clubname FROM tbl_clubs WHERE delete_status = 0
    ORDER BY clubname`
  );
  return rows.map(row => new ClubName(row));
}

async function listTeacher() {
  const [rows] = await pool.query(
    `SELECT tid,concat(firstname, ' ', lastname) AS teacher_name 
    FROM tbl_teachers WHERE delete_status = 0  ORDER BY teacher_name`
  );
  return rows.map(row => new Teacher(row));
}

async function listStudent() {
  const [rows] = await pool.query(
    `SELECT sid,concat(firstname, ' ', lastname) AS student_name 
    FROM tbl_students WHERE delete_status = 0  ORDER BY student_name`
  );
  return rows.map(row => new Student(row));
}

async function listTa() {
  const [rows] = await pool.query(
    `SELECT taid,concat(firstname, ' ', lastname) AS ta_name 
    FROM tbl_tas WHERE delete_status = 0  ORDER BY ta_name`
  );
  return rows.map(row => new TA(row));
}

async function lisPartner() {
  const [rows] = await pool.query(
    `SELECT pid,partner_name
    FROM tbl_partners WHERE delete_status = 0  ORDER BY partner_name`
  );
  return rows.map(row => new Partner(row));
}

async function listData(id, checktype) {
  if (id == "1" || id == "7") {
    if (checktype == "TA" || !checktype) {
      const [rows] = await pool.query(
        `SELECT *,taid AS id,"1" AS user_type,"TA" as checktype FROM tbl_tas WHERE delete_status = 0`
      );
      return rows.map(row => new Club(row));
    } else {
      const [rows] = await pool.query(
        `SELECT *,tid AS id,"7" AS user_type,"Teacher" as checktype FROM tbl_teachers WHERE delete_status = 0`
      );
      return rows.map(row => new Club(row));
    }
  } else if (id == "2") {
    const [rows] = await pool.query(
      `SELECT *,sid AS id,"2" AS user_type FROM tbl_students WHERE delete_status = 0`
    );
    return rows.map(row => new Club(row));
  } else if (id == "3") {
    const [rows] = await pool.query(
      `SELECT cs.id,c.clubname,concat(s.firstname, " " , s.lastname) as firstname
      ,DATE_FORMAT(cs.create_date,'%d/%m/%Y') as create_date
      ,"3" AS user_type FROM tbl_check_student cs INNER JOIN tbl_students s on cs.sid = s.sid
      INNER JOIN tbl_clubs c on cs.cid = c.cid
       WHERE cs.delete_status = 0`
    );
    return rows.map(row => new Club(row));
  } else if (id == "4") {
    const [rows] = await pool.query(
      `SELECT cid AS id,day,clubname AS firstname,"4" AS user_type,create_by
       ,substring(start_time,1,5) as start_time,substring(end_time,1,5) as end_time
       FROM tbl_clubs WHERE delete_status = 0`
    );
    return rows.map(row => new Club(row));
  } else if (id == "5") {
    const [rows] = await pool.query(
      `SELECT *,pid AS id,partner_name AS firstname,"5" AS user_type FROM tbl_partners WHERE delete_status = 0`
    );
    return rows.map(row => new Club(row));
  } else if (id == "6") {
    const [rows] = await pool.query(
      `SELECT distinct c.cid AS id,c.clubname,c.day,substring(c.start_time,1,5) as start_time
      ,substring(c.end_time,1,5) as end_time,"6" AS user_type      
       FROM tbl_club_reltionship cr inner join tbl_clubs c on cr.cid = c.cid 
       where cr.delete_status = 0`
    );
    return rows.map(row => new Club(row));
  }
}

async function ckeckCount(ddl_check_clubname) {
  const [rows] = await pool.query(
    `SELECT count(cid) AS check_club FROM tbl_check_student 
  WHERE cid = ? AND delete_status = 0 and 
  DATE_FORMAT(create_date, '%d/%m/%Y') = DATE_FORMAT(now(),'%d/%m/%Y')`,
    [ddl_check_clubname]
  );
  return rows.map(row => new CountStudent(row));
}

async function deleteData(id, type, checktype) {
  if (type == "1") {
    if (checktype == "TA") {
      {
        const result = await pool.query(
          `
              update tbl_tas set delete_status = 1
              where taid = ?
          `,
          [id]
        );
        return result;
      }
    } else {
      {
        const result = await pool.query(
          `
              update tbl_teachers set delete_status = 1
              where tid = ?
          `,
          [id]
        );
        return result;
      }
    }
  } else if (type == "2") {
    {
      const result = await pool.query(
        `
            update tbl_students set delete_status = 1
            where sid = ?
        `,
        [id]
      );
      return result;
    }
  } else if (type == "3") {
    const result = await pool.query(
      `
          update tbl_check_student set delete_status = 1
          where id = ?
      `,
      [id]
    );
    return result;
  } else if (type == "4") {
    {
      const result = await pool.query(
        `
            update tbl_clubs set delete_status = 1
            where cid = ?
        `,
        [id]
      );
      return result;
    }
  } else if (type == "5") {
    {
      const result = await pool.query(
        `
            update tbl_partners set delete_status = 1
            where pid = ?
        `,
        [id]
      );
      return result;
    }
  } else if (type == "6") {
    {
      const result = await pool.query(
        `
            update tbl_club_reltionship set delete_status = 1
            where cid = ?
        `,
        [id]
      );
      return result;
    }
  }
}
async function viewStudent(id) {
  const [rows] = await pool.query(
    `SELECT DISTINCT concat(s.firstname," ",s.lastname) AS student_detail,c.clubname AS clubname_detail
    ,c.day AS day_detail,concat(t.firstname," ",t.lastname) AS teacher_detail 
     FROM tbl_club_reltionship cr INNER join tbl_students s on cr.sid = s.sid 
     INNER JOIN tbl_clubs c ON cr.cid = c.cid INNER JOIN tbl_teachers t on cr.tid = t.tid
     WHERE cr.delete_status = 0 AND cr.cid = ?`,
    [id]
  );
  return rows.map(row => new StudentDetail(row));
}

async function viewTA(id) {
  const [rows] = await pool.query(
    `SELECT DISTINCT concat(ta.firstname," ",ta.lastname) AS ta_detail  
    FROM tbl_club_reltionship cr INNER join tbl_tas ta on cr.taid = ta.taid 
    WHERE cr.delete_status = 0 AND cr.cid = ?`,
    [id]
  );
  return rows.map(row => new TaDetail(row));
}

async function viewPartner(id) {
  const [rows] = await pool.query(
    `SELECT DISTINCT p.partner_name AS partner_detail 
    FROM tbl_club_reltionship cr INNER join tbl_partners p on cr.pid = p.pid 
    WHERE cr.delete_status = 0 AND cr.cid = ?`,
    [id]
  );
  return rows.map(row => new PartnerDetail(row));
}

async function updateData(id, type, data) {
  if (type == "1") {
    if (data.checktype == "TA") {
      {
        const result = await pool.query(
          `
              update tbl_tas set firstname = ?,lastname = ?,tel = ?,email = ? 
              where taid = ?
          `,
          [ta_firstname, ta_lastname, ta_telephone, ta_email, id]
        );
        return result;
      }
    } else if (data.checktype == "Teacher") {
      {
        const result = await pool.query(
          `
              update tbl_teachers set firstname = ?,lastname = ? ,tel = ?,email= ? 
              where tid = ?
          `,
          [ta_firstname, ta_lastname, ta_telephone, ta_email, id]
        );
        return result;
      }
    }
  } else if (type == "2") {
    {
      const result = await pool.query(
        `
            update tbl_students set firstname = ?,lastname = ?,year = ? 
            where sid = ?
        `,
        [student_firstname, student_lastname, student_year, id]
      );
      return result;
    }
  } else if (type == "3") {
    //check student
  } else if (type == "4") {
    {
      const result = await pool.query(
        `
            update tbl_clubs set clubname = ?,day = ?,start_time = ?,end_time = ?
            where cid = ?
        `,
        [clubname, club_day, start_time, end_time, id]
      );
      return result;
    }
  } else if (type == "5") {
    {
      const result = await pool.query(
        `
            update tbl_partners set partner_name = ?,email = ?,tel = ?,contact_person = ?
            where pid = ?
        `,
        [partner_name, partner_email, partner_tel, personal_contact, id]
      );
      return result;
    }
  } else if (type == "6") {
    //club relationship
  } else if (type == "7") {
    {
      const result = await pool.query(
        `
            update tbl_teachers set firstname='${ta_firstname}',lastname='${ta_lastname}',tel=${ta_telephone},email='${ta_email}'  
            where tid = ${id}
        `
      );
      return result;
    }
  }
}

async function DeleteListClub(checkclub) {
  const result = await pool.query(
    `DELETE FROM tbl_check_student WHERE cid = ? 
    AND DATE_FORMAT(create_date, '%d/%m/%Y') = DATE_FORMAT(now(),'%d/%m/%Y')`,
    [checkclub]
  );
  return result;
}

module.exports = {
  registerTAOrTeacher,
  registerStudent,
  registerCheckStudent,
  registerClub,
  registerPartner,
  registerRelationship,
  listData,
  listClub,
  listTeacher,
  listStudent,
  listTa,
  lisPartner,
  deleteData,
  viewStudent,
  viewTA,
  viewPartner,
  updateData,
  ckeckCount,
  DeleteListClub
};
