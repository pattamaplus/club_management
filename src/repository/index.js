const user = require("./user");
const club = require("./club");

module.exports = {
  user,
  club
};
