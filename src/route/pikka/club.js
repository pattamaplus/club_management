const { club } = require("../../repository");

let data = {};

const getHandler = async ctx => {
  await ctx.render("club");
};

const getListClub = async ctx => {
  if (ctx.params.id == "1" || ctx.params.id == "7" || !ctx.params.id) {
    const { checktype } = ctx.request.body;
    const id = ctx.params.id ? ctx.params.id : "1";
    let type = checktype ? checktype : "TA";

    if (ctx.params.id == "7") {
      type = "Teacher";
    }
    const clubObject = await club.listData(id, type);
    data.clubs = clubObject;
    data.user_types = { user_type: ctx.params.id };
    data.club_id = { cid: 0 };
  } else if (
    ctx.params.id != "1" &&
    ctx.params.id != "3" &&
    ctx.params.id != "6" &&
    ctx.params.id
  ) {
    const clubObject = await club.listData(ctx.params.id);
    data.clubs = clubObject;
    data.user_types = { user_type: ctx.params.id };
    data.club_id = { cid: 0 };
  } else if (ctx.params.id == "3") {
    const clubObject = await club.listData(ctx.params.id);
    const clubName = await club.listClub();
    const StudentObject = await club.listStudent();
    data.clubs = clubObject;
    data.clubNames = clubName;
    data.students = StudentObject;
    data.user_types = { user_type: ctx.params.id };
    data.club_id = { cid: 0 };
  } else if (ctx.params.id == "6") {
    const clubObject = await club.listData(ctx.params.id);
    const clubName = await club.listClub();
    const TeacherObject = await club.listTeacher();
    const StudentObject = await club.listStudent();
    const TaObject = await club.listTa();
    const PartnerObject = await club.lisPartner();

    data.clubs = clubObject;
    data.clubNames = clubName;
    data.teachers = TeacherObject;
    data.students = StudentObject;
    data.tas = TaObject;
    data.partners = PartnerObject;
    data.user_types = { user_type: ctx.params.id };

    if (ctx.params.cid != "0") {
      const studentDetail = await club.viewStudent(ctx.params.cid);
      const taDetail = await club.viewTA(ctx.params.cid);
      const partnerDetail = await club.viewPartner(ctx.params.cid);
      data.student_detail = studentDetail;
      data.ta_details = taDetail;
      data.partner_deatils = partnerDetail;
      data.club_id = { cid: ctx.params.cid };
    } else {
      data.club_id = { cid: 0 };
    }
  }
  await ctx.render("club", data);
};

const postHandler = async ctx => {
  const {
    activities,
    checktype,
    ta_firstname,
    ta_lastname,
    ta_telephone,
    ta_email,
    student_firstname,
    student_lastname,
    student_year,
    clubname,
    club_day,
    start_time,
    end_time,
    partner_name,
    aptitude,
    personal_contact,
    partner_tel,
    partner_email,
    ddl_clubname,
    ddl_teacher,
    ddl_student,
    ddl_ta,
    ddl_partner,
    ddl_check_clubname,
    ddl_check_student
  } = ctx.request.body;

  const create_by = ctx.session.userId;
  //console.log("activities : " + activities);
  if (activities == "1") {
    const tid = await club.registerTAOrTeacher(
      ta_firstname,
      ta_lastname,
      ta_telephone,
      ta_email,
      create_by,
      checktype
    );
  } else if (activities == "2") {
    const sid = await club.registerStudent(
      student_firstname,
      student_lastname,
      student_year,
      create_by
    );
  } else if (activities == "3") {
    const check = await club.ckeckCount(ddl_check_clubname);

    if (check[0].check_club != 0) {
      const deleteClub = await club.DeleteListClub(ddl_check_clubname);
      const id = await club.registerCheckStudent(
        ddl_check_clubname,
        ddl_check_student,
        create_by
      );
    } else {
      const id = await club.registerCheckStudent(
        ddl_check_clubname,
        ddl_check_student,
        create_by
      );
    }
  } else if (activities == "4") {
    const cid = await club.registerClub(
      clubname,
      club_day,
      start_time,
      end_time,
      create_by
    );
  } else if (activities == "5") {
    const pid = await club.registerPartner(
      partner_name,
      aptitude,
      personal_contact,
      partner_tel,
      partner_email,
      create_by
    );
  } else if (activities == "6") {
    const rid = await club.registerRelationship(
      ddl_clubname,
      ddl_teacher,
      ddl_student,
      ddl_ta,
      ddl_partner,
      create_by
    );
  }

  if (activities == "1" && checktype == "Teacher") {
    await ctx.redirect(`/club/7/0`);
  } else {
    await ctx.redirect(`/club/${activities}/0`);
  }
};

const postDeleteHandler = async ctx => {
  const { activities, checktype } = ctx.request.body;
  console.log("test");
  if (activities == "1") {
    const result = await club.deleteData(ctx.params.id, activities, checktype);
    console.log(result);
  } else if (activities != "1") {
    const result = await club.deleteData(ctx.params.id, activities, "");
    console.log(result);
  }
  if (activities == "1" && checktype == "Teacher") {
    await ctx.redirect("../../club/7/0");
  } else {
    await ctx.redirect(`../../club/${activities}/0`);
  }
};

const postEditHandler = async ctx => {
  const { activities, select_ta_teacher, checktype } = ctx.request.body;
  console.log(select_ta_teacher);
  if (activities == "1") {
    const data = ({
      ta_firstname,
      ta_lastname,
      ta_telephone,
      ta_email
    } = ctx.request.body);
    const result = await club.updateData(ctx.params.id, activities, data);
  } else if (activities == "2") {
    const data = ({
      student_firstname,
      student_lastname,
      student_year
    } = ctx.request.body);
    if (data.student_year != 0) {
      const result = await club.updateData(ctx.params.id, activities, data);
    }
  } else if (activities == "3") {
    // check student
    const data = ({
      hidden_id,
      ddl_check_clubname,
      ddl_check_student
    } = ctx.request.body);
    console.log(data);
  } else if (activities == "4") {
    const data = ({
      clubname,
      club_day,
      start_time,
      end_time
    } = ctx.request.body);
    if (data.club_day != 0) {
      const result = await club.updateData(ctx.params.id, activities, data);
    }
  } else if (activities == "5") {
    const data = ({
      partner_name,
      partner_tel,
      partner_email,
      personal_contact
    } = ctx.request.body);
    const result = await club.updateData(ctx.params.id, activities, data);
  } else if (activities == "6") {
    //club relationship
  } else if (activities == "7") {
    const data = ({
      ta_firstname,
      ta_lastname,
      ta_telephone,
      ta_email
    } = ctx.request.body);
    const result = await club.updateData(ctx.params.id, activities, data);
  }
  const clubObject = await club.listData(activities);
  data.clubs = clubObject;

  if (activities == "1" && checktype == "Teacher") {
    await ctx.redirect("../../club/7/0");
  } else {
    await ctx.redirect(`../../club/${activities}/0`);
  }
};

module.exports = {
  getHandler,
  postHandler,
  getListClub,
  postDeleteHandler,
  postEditHandler
};
