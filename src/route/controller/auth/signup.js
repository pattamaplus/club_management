const { user } = require("../../../repository");

const getHandler = ctx => {
  ctx.body = "sign up get handler";
};

const postHandler = async ctx => {
  const { username, password } = ctx.request.body;
  const userId = await user.register(username, password);
};

module.exports = {
  getHandler,
  postHandler
};
