const { user } = require("../../repository");
const { club } = require("../../repository");
const md5 = require("md5");

let data = {};

const getHandler = async ctx => {
  data.flash = ctx.session.flash;
  await ctx.render("signin");
};

const postHandler = async ctx => {
  const users = await user.login(ctx.request.body.username);
  //console.log(JSON.stringify(users));
  let hashInputPassword = await md5(ctx.request.body.password);
  //const same = false;
  //if (inputPass == users.password) console.log("issame"); else console.log("not same")

  if (hashInputPassword != users.password) {
    ctx.session.flash = { error: "username or password is wrong" };
    data.flash = ctx.session.flash;
    return ctx.redirect("signin");
  } else {
    ctx.session.userId = users.uid;
    const clubObject = await club.listData(1);
    data.clubs = clubObject;
    data.flash = ctx.session.flash;
    await ctx.redirect("/club/1/0");
  }
};

module.exports = {
  getHandler,
  postHandler
};
