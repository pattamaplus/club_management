const { user } = require("../../repository");
const md5 = require("md5");
const flash = require("flash");

let data = {};

const getHandler = async ctx => {
  data.flash = ctx.session.flash;
  await ctx.render("signup");
};

const postHandler = async ctx => {
  const { username, password, confirmPassword } = ctx.request.body;
  if (password.length < 6 || confirmPassword.length < 6) {
    ctx.session.flash = { error: "Password must be at least 6 characters." };
    return ctx.redirect("signup");
  }

  if (password !== confirmPassword) {
    //console.log("Password does not match!!");
    ctx.session.flash = { error: "Password does not match!!" };
    data.flash = ctx.session.flash;
    return ctx.redirect("signup");
  } else {
    const hashedPassword = await md5(password);
    const userId = await user.register(username, hashedPassword);
  }

  data.flash = ctx.session.flash;
  ctx.redirect("signin");
};

module.exports = {
  getHandler,
  postHandler
};
